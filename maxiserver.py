#!/usr/bin/env python

import os
import re
from time import sleep
from datetime import datetime
from subprocess import Popen, PIPE
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer

MAX_AMOUNT_DEMOS = 3 # Number determined after simply testing how far we
                     # could go. With 5 running demos it starts swapping.
                     # With 4 running demos it starts swapping when you try
                     # to stop them all simultaneously.
                     # Turning the machine to a zombie, so this is something
                     # to avoid at all cause.
PORT_NUMBER = 30080 # Port of this webserver. The demos will run on 2000*.
COMMON_DEMO_DIR = "/home/ubuntu/demo-generic"
GENERIC_DEMO_DIR = "/home/ubuntu/demo-generic/integration-1.2.0-{}"
SERVER_LOG_FILE = "maxiserver.log"
UP_LOG_FILE = "up.log"
DOWN_LOG_FILE = "down.log"
TIMESTAMP_FORMAT = "%Y-%m-%dT%H:%M:%S"

def define_collections(server):
    # Our list of demos, stored as a list of strings of format
    # "00", "01", "02",... We call these the demo_ids.
    # For now we just assume a maximum of MAX_AMOUNT_DEMOS simultaneous
    # demos.
    # To facilitate port management, we foresee n servers, with
    # port range 20000..20000+n, mapping to demo_ids "00" to n.
    # We keep a list of what was started.
    # An id can only leave the list if it's explicitely stopped.
    server.started = []
    # We also keep a list of demos to remember what was running.
    # An id can only leave the list if it's confirmed that it's down
    # and it's not in started.
    server.running = []

class myHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.generate_main_page()

    def do_POST(self):
        self.do_POST_action()
        self.do_GET()

    def do_POST_action(self):
        post_args = self.rfile.read(int(self.headers['content-length']))
        if "UP" in post_args:
            self.spin_up_new_demo()
            return
        if "STOPALL" in post_args:
            self.stop_all_demos() # Stopping them all
            return
        # Using regular expression:
        re_result = re.search("(STOP)(\d\d)", post_args) # Like "STOP02"
        if re_result:
            self.stop_demo(re_result.group(2)) # Stopping "02"
            return

    def generate_main_page(self):
        actions = self.generate_actions_html();
        status = self.generate_all_status_html();
        content = "{}\n{}".format(actions, status)
        self.show_page(content)

    def show_page(self, content):
        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        html_base = ("<!DOCTYPE html>\n<html>\n<head>\n<title>{}</title>"
                     "\n<meta http-equiv=\"refresh\" content=\"5\">"
                     "\n</head>\n<body>\n{}\n</body>\n</html>")
        self.wfile.write(html_base.format("Mender test on AWS", content))

    def log(self, message):
        timestamp = datetime.now().strftime(TIMESTAMP_FORMAT)
        with open(OUTPUT_FILE, 'a') as log_file:
            log_file.write('%s\t%s\n' % (timestamp, message))

    def generate_actions_html(self):
        html = ("<h3>Actions</h3>\n<form action=\"\" method =\"post\">"
                "\n{}\n<br><br>\n{}\n</form>")
        button = ("<input type=\"submit\" {} name=\"{}\" value=\"{}\"/>")
        button_status = "" # Enabled
        if (len(self.server.running) >= MAX_AMOUNT_DEMOS):
            button_status = "disabled"
        label = "Spin up a demo server"
        button_up = button.format(button_status, "UP", label)
        button_status = "disabled"
        if (self.server.running):
            button_status = "" # Enabled
        label = "Stop all the running demo servers"
        button_down = button.format(button_status, "STOPALL", label)
        return html.format(button_up, button_down)

    def generate_all_status_html(self):
        demos = list(self.server.running) # Copy, because while checking
                                          # statuses we might change it
        if demos:
            statuses = [self.generate_status_html(demo_id) for demo_id in demos]
            return "\n".join(statuses)
        else:
            # Demo list is empty: nothing is running
            return "<h3>Status</h3>\nCurrently no demos are running."
        running_status = "No containers for the mender demo are running."
        if running:
            running_status = "Containers for the mender demo are running."
        serving_status = "Webserver is not serving anything."
        if serving:
            serving_status = ("Webserver is serving. Try: <a href=\""
                              "https://18.194.110.97\" target=\"_blank\"/>"
                              "https://18.194.110.97</a>")
        return html.format(running_status, serving_status)

    def generate_status_html(self, demo_id):
        running = we_are_running(demo_id)
        serving = we_are_serving(demo_id)
        if (demo_id in self.server.started) or (running or serving):
            # The status part:
            html = ("<h3>Status for demo {}</h3>\n"
                    "<ul>\n<li>{}</li>\n<li>{}</li>\n</ul>")
            running_status = "No containers for the mender demo are running."
            if running:
                running_status = "Containers for the mender demo are running."
            serving_status = "Webserver is not serving anything."
            if serving:
                serving_status = ("Webserver is serving. Try: <a href=\""
                        "https://18.194.110.97:200{}/ui/\" target=\"_blank\"/>"
                        "https://18.194.110.97:200{}/ui/</a> "
                        "(username created: demo{}@host.com)")
                serving_status = serving_status.format(demo_id,
                                                       demo_id,
                                                       demo_id)
            status = html.format(demo_id, running_status, serving_status)
            # The buttons part:
            ##form = ("<form action=\"\" method =\"post\">"
            ##        "\n{}\n<br><br>\n{}\n</form>")
            form = ("<form action=\"\" method =\"post\">"
                    "\n{}\n</form>")
            button = ("<input type=\"submit\" name=\"{}\" value=\"{}\"/>")
            label = "Stop this server"
            button_stop = button.format("STOP{}".format(demo_id), label)
            # Not yet implemented...
            ##button = ("<input type=\"button\" value=\"{}\" "
            ##          "onclick=\"window.open('{}')\">")
            ##label = "Show logs"
            ##button_log = button.format(label, "logs{}".format(demo_id))
            ##buttons = form.format(button_stop, button_log)
            buttons = form.format(button_stop)
            # The warning message part:
            if demo_id not in self.server.started:
              warning = ("<ul>\n<li>This server was stopped... "
                         "please be patient.</li>\n</ul>")
              return "\n".join([status, buttons, warning])
            return "\n".join([status, buttons])
        else: # running nor serving and also not in the the list of started
              # -> remove it from the list of running servers!
            self.server.running.remove(demo_id)
            return "<h3>Demo {} is not running anymore.</h3>\n".format(demo_id)

    def spin_up_new_demo(self):
        # First find an unused number:
        for i in range(MAX_AMOUNT_DEMOS):
            demo_id = "{:02d}".format(i)
            if demo_id not in self.server.running:
                # These are just some extra tests, just to be sure
                # Doesn't hurt
                running = we_are_running(demo_id)
                serving = we_are_serving(demo_id)
                if not (running or serving):
                    # Add it to the list:
                    # Doing this first, so that it's at least monitored when
                    # anything fails
                    self.server.started.append(demo_id)
                    self.server.running.append(demo_id)
                    # Generate the demo:
                    run_make_demo_script(demo_id)
                    # Spin it up:
                    run_up_script(demo_id)
                    break

    def stop_all_demos(self):
        for demo_id in self.server.running:
            self.stop_demo(demo_id)

    def stop_demo(self, demo_id):
        # We remove it from the list of started demos
        # This means that we wil still see its status,
        # because it remains in the list of running demos.
        # But because it's not in started anymore, it will be
        # removed from running if we have confirmation that it's indeed
        # stopped.
        if demo_id in self.server.started:
            self.server.started.remove(demo_id)
        run_stop_script(demo_id)

############################################################################
# Methods to check the server status:

def we_are_running(demo_id):
    filter_str = "name=integration120{}".format(demo_id)
    proc = Popen(["docker", "ps", "-q", "-f", filter_str],
                 stdout=PIPE, stderr=PIPE)
    output = proc.communicate()[0].strip()
    return (output is not "")

def we_are_serving(demo_id):
    url = "https://localhost:200{}/ui/".format(demo_id)
    proc = Popen(["curl", "--insecure", url],
                  stdout=PIPE, stderr=PIPE)
    output = proc.communicate()[0]
    serving = ("Mender" in output)
    # It's not the most efficient thing to do, but we try to create the user,
    # when we know that the webserver is up.
    if serving:
        generate_user(demo_id)
    return serving


############################################################################
# Methods that run scripts:

def run_make_demo_script(demo_id):
    working_dir = COMMON_DEMO_DIR
    proc = Popen(["./make-demo", demo_id], cwd=working_dir)
    proc.communicate()[0]
    # Note: more ideally, we would check the output here.

def run_up_script(demo_id):
    working_dir = GENERIC_DEMO_DIR.format(demo_id)
    log = open(os.path.join(working_dir, UP_LOG_FILE), "a")
    proc = Popen(["./up"], cwd=working_dir, stdout=log, stderr=log)
    ## sleep(1) # No real reason for this anymore

def generate_user(demo_id):
    working_dir = GENERIC_DEMO_DIR.format(demo_id)
    username = "--username=demo{}@host.com".format(demo_id)
    password = "--password=finse1222"
    command = ["./demo", "exec", "mender-useradm", "/usr/bin/useradm",
               "create-user", username, password]
    proc = Popen(command, cwd=working_dir, stdout=PIPE, stderr=PIPE)
    proc.communicate()[0]
    # Note: more ideally, we would check the output here.

def run_stop_script(demo_id):
    if we_are_running(demo_id):
        working_dir = GENERIC_DEMO_DIR.format(demo_id)
        log = open(os.path.join(working_dir, DOWN_LOG_FILE), "a")
        proc = Popen(["./stop"], cwd=working_dir, stdout=log, stderr=log)
        ## sleep(1) # No real reason for this anymore


# The script's main loop:
try:
    # IMPORTANT:
    # We assume that when the server is started, all is clean and no demos
    # are running.
    # It should be the case anyway, since the demos stop when you
    # stop the server.

    # Set environment variable(s)
    os.environ["COMPOSE_HTTP_TIMEOUT"] = "600" # Needed to stop demo from
                                               # stopping after ~60s
    # Create a web server and define the handler to manage the
    # incoming request
    server = HTTPServer(('', PORT_NUMBER), myHandler)
    print "Started httpserver on port " , PORT_NUMBER

    define_collections(server)

    # Wait forever for incoming http requests
    server.serve_forever()

except KeyboardInterrupt:
    print "^C received, shutting down the web server"
    server.socket.close()
