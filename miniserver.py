#!/usr/bin/env python

import os
from time import sleep
from datetime import datetime
from subprocess import Popen, PIPE
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer

PORT_NUMBER = 20080
MENDER_TEST_ENV_DIR = "/home/ubuntu/integration-1.2.0"
LOG_FILE = "miniserver.log"
TIMESTAMP_FORMAT = "%Y-%m-%dT%H:%M:%S"


class myHandler(BaseHTTPRequestHandler):
    #Handler for the GET requests
    def do_GET(self):
        if self.path == "/test":
            proc = Popen(["ls","-l"], stdout=PIPE)
            output = proc.communicate()[0]
            self.show_page(some_static_test_method() + "Testing something: " + output)
            self.log("Testing something")
        else:
            self.generate_main_page()

    def do_POST(self):
        post_args = self.rfile.read(int(self.headers['content-length']))
        if "UP" in post_args:
            run_up_script()
        elif "STOP" in post_args:
            run_stop_script()
        self.do_GET()

    def generate_main_page(self):
        running = we_are_running_something()
        serving = we_are_serving_something()
        status = generate_status_html(running, serving);
        actions = generate_actions_html(running, serving);
        content = "{}\n{}".format(status, actions)
        self.show_page(content)

    def show_page(self, content):
        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        html_base = ("<!DOCTYPE html>\n<html>\n<head>\n<title>{}</title>"
                     "\n<meta http-equiv=\"refresh\" content=\"5\">"
                     "\n</head>\n<body>\n{}\n</body>\n</html>")
        self.wfile.write(html_base.format("Mender test on AWS", content))

    def log(self, message):
        timestamp = datetime.now().strftime(TIMESTAMP_FORMAT)
        with open(OUTPUT_FILE, 'a') as log_file:
            log_file.write('%s\t%s\n' % (timestamp, message))

def we_are_running_something():
    proc = Popen(["docker", "ps", "-q", "-f", "name=integration120_*"],
                 stdout=PIPE, stderr=PIPE)
    output = proc.communicate()[0].strip()
    return (output is not "")

def we_are_serving_something():
    proc = Popen(["curl", "--insecure", "https://localhost/ui/"],
                  stdout=PIPE, stderr=PIPE)
    output = proc.communicate()[0]
    return ("Mender" in output)

def generate_status_html(running, serving):
    html = "<h3>Status</h3>\n<ul>\n<li>{}</li>\n<li>{}</li>\n</ul>"
    running_status = "No containers for the mender demo are running."
    if running:
        running_status = "Containers for the mender demo are running."
    serving_status = "Webserver is not serving anything."
    if serving:
        serving_status = ("Webserver is serving. Try: <a href=\""
                          "https://18.194.110.97\" target=\"_blank\"/>"
                          "https://18.194.110.97</a>")
    return html.format(running_status, serving_status)

def generate_actions_html(running, serving):
    html = ("<h3>Actions</h3>\n<form action=\"\" method =\"post\">"
            "\n{}\n<br><br>\n{}\n</form>")
    button = ("<input type=\"submit\" {} name=\"{}\" value=\"{}\"/>")
    button_status = "" # Enabled
    if (running or serving):
        button_status = "disabled"
    label = "Spin up a demo server"
    button_up = button.format(button_status, "UP", label)
    button_status = "disabled"
    if (running):
        button_status = "" # Enabled
    label = "Stop the running demo server"
    button_down = button.format(button_status, "STOP", label)
    return html.format(button_up, button_down)

def run_up_script():
    running = we_are_running_something()
    serving = we_are_serving_something()
    if not (running or serving):
        proc = Popen(["./up"], cwd=MENDER_TEST_ENV_DIR)
        sleep(1)

def run_stop_script():
    if we_are_running_something():
        proc = Popen(["./stop"], cwd=MENDER_TEST_ENV_DIR)
        sleep(1)


# The script's main loop:
try:
    # IMPORTANT:
    # We assume that when the server is started, all is clean and no demos
    # are running.
    # If this cannot be guaranteed, we could write a method to do that here.
    # Set environment variable(s)
    os.environ["COMPOSE_HTTP_TIMEOUT"] = "600" # Needed to stop demo from
                                               # hanging after ~60s
    # Create a web server and define the handler to manage the
    # incoming request
    server = HTTPServer(('', PORT_NUMBER), myHandler)
    print "Started httpserver on port " , PORT_NUMBER
    # Wait forever for incoming http requests
    server.serve_forever()

except KeyboardInterrupt:
    print "^C received, shutting down the web server"
    server.socket.close()
